// This file is part of Elastic. License: GPL-3.0+.

[GtkTemplate (ui = "/app/drey/Elastic/properties-view.ui")]
public class Elastic.PropertiesView : Adw.Bin {
    private const double MIN_DAMPING_RATIO = 0.01;
    private const double MAX_DAMPING_RATIO = 1.5;
    private const double DEFAULT_DAMPING_RATIO = 0.5;
    private const double CRIT_DAMPING_RATIO = 1.0;

    private SpringParams _spring;
    public SpringParams spring {
        get { return _spring; }
        set {
            if (spring == value)
                return;

            if (spring != null)
                spring.notify["damping-ratio"].disconnect (sync_damping);

            _spring = value;

            if (spring != null)
                spring.notify["damping-ratio"].connect (sync_damping);

            sync_damping ();
        }
    }

    public bool use_damping { get; set; }

    private bool _compact;
    public bool compact {
        get { return _compact; }
        set {
            if (compact == value)
                return;

            _compact = value;

            multi_layout_view.layout_name = compact ? "narrow" : "wide";
        }
    }

    [GtkChild]
    private unowned Adw.MultiLayoutView multi_layout_view;
    [GtkChild]
    private unowned Gtk.Stack damping_stack;
    [GtkChild]
    private unowned PropertyRow damping_row;
    [GtkChild]
    private unowned PropertyRow damping_ratio_row;
    [GtkChild]
    private unowned PropertyRow mass_row;
    [GtkChild]
    private unowned PropertyRow stiffness_row;
    [GtkChild]
    private unowned PropertyRow epsilon_row;

    private BindingGroup spring_bindings;

    private Settings settings;

    construct {
        settings = new Settings ("app.drey.Elastic");

        settings.bind ("use-damping", this, "use-damping", GET);

        spring_bindings = new BindingGroup ();

        spring_bindings.bind (
            "damping", damping_row,
            "value", SYNC_CREATE | BIDIRECTIONAL
        );
        spring_bindings.bind (
            "damping-ratio", damping_ratio_row,
            "value", SYNC_CREATE | BIDIRECTIONAL
        );
        spring_bindings.bind (
            "mass", mass_row,
             "value", SYNC_CREATE | BIDIRECTIONAL
         );
        spring_bindings.bind (
            "stiffness", stiffness_row,
            "value", SYNC_CREATE | BIDIRECTIONAL
        );
        spring_bindings.bind (
            "epsilon", epsilon_row,
            "value", SYNC_CREATE | BIDIRECTIONAL
        );

        bind_property ("spring", spring_bindings, "source", DEFAULT);

        notify["use-damping"].connect (() => {
            damping_stack.visible_child = use_damping ? damping_row : damping_ratio_row;
        });

        damping_stack.visible_child = use_damping ? damping_row : damping_ratio_row;
    }

    static construct {
        set_css_name ("properties-view");
    }

    private void sync_damping () {
        damping_row.lower = spring.damping_for_damping_ratio (MIN_DAMPING_RATIO);
        damping_row.upper = spring.damping_for_damping_ratio (MAX_DAMPING_RATIO);
        damping_row.mark = spring.damping_for_damping_ratio (DEFAULT_DAMPING_RATIO);
        damping_row.secondary_mark = spring.damping_for_damping_ratio (CRIT_DAMPING_RATIO);

        damping_row.value = damping_row.value.clamp (
            damping_row.lower, damping_row.upper
        );
    }
}
