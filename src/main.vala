// This file is part of Elastic. License: GPL-3.0+.

int main (string[] args) {
    Intl.bindtextdomain (Config.GETTEXT_PACKAGE, Config.GNOMELOCALEDIR);
    Intl.bind_textdomain_codeset (Config.GETTEXT_PACKAGE, "UTF-8");
    Intl.textdomain (Config.GETTEXT_PACKAGE);

    GtkSource.init ();

    var app = new Elastic.Application ();
    return app.run (args);
}
