// This file is part of Elastic. License: GPL-3.0+.

[GtkTemplate (ui = "/app/drey/Elastic/transforms-view.ui")]
public class Elastic.TransformsView : Adw.Bin {
    private SpringParams _spring;
    public SpringParams spring {
        get { return _spring; }
        set {
            if (spring == value)
                return;

            if (spring != null)
                spring.changed.disconnect (reset_animation);

            _spring = value;

            if (spring != null)
                spring.changed.connect (reset_animation);

            reset_animation ();
        }
    }

    private bool _compact;
    public bool compact {
        get { return _compact; }
        set {
            if (compact == value)
                return;

            _compact = value;

            multi_layout_view.layout_name = compact ? "narrow" : "wide";
        }
    }

    [GtkChild]
    private unowned Adw.MultiLayoutView multi_layout_view;
    [GtkChild]
    private unowned TransformBin scale_bin;
    [GtkChild]
    private unowned TransformBin htranslate_bin;
    [GtkChild]
    private unowned TransformBin rotate_bin;
    [GtkChild]
    private unowned TransformBin vtranslate_bin;

    private Adw.SpringAnimation animation;
    private bool invert;

    construct {
        animation = new Adw.SpringAnimation (
            this, 0, 1, new Adw.SpringParams.full (1, 1, 1),
            new Adw.CallbackAnimationTarget (set_value)
        );

        animation.follow_enable_animations_setting = false;

        set_value (0);
    }

    static construct {
        set_css_name ("transforms-view");
    }

    public void run_animation () {
        spring.apply (animation);

        animation.value_from = animation.value;
        animation.value_to = invert ? 0 : 1;

        animation.play ();

        invert = !invert;
    }

    private void set_value (double value) {
        float x = (float) Adw.lerp (-30, 30, value);
        float y = (float) Adw.lerp (30, -30, value);
        float s = float.max (0, (float) Adw.lerp (3, 1, value));
        float r = (float) Adw.lerp (0, 90, value);

        htranslate_bin.transform = new Gsk.Transform ().translate ({ x, 0 });
        vtranslate_bin.transform = new Gsk.Transform ().translate ({ 0, y });
        scale_bin.transform = new Gsk.Transform ().scale (s, s);
        rotate_bin.transform = new Gsk.Transform ().rotate (r);
    }

    private void reset_animation () {
        animation.value_from = invert ? 1 : 0;

        animation.reset ();
    }

    protected override void unmap () {
        base.unmap ();

        reset_animation ();
    }
}
