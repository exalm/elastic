// This file is part of Elastic. License: GPL-3.0+.

public class Elastic.TransformBin : Adw.Bin {
    public Gsk.Transform? transform { get; set; }

    construct {
        layout_manager = null;

        notify["transform"].connect (queue_allocate);
    }

    static construct {
        set_css_name ("transform-bin");
    }

    protected override void measure (
        Gtk.Orientation orientation,
        int for_size,
        out int minimum,
        out int natural,
        out int minimum_baseline,
        out int natural_baseline
    ) {
        if (child == null) {
            minimum = 0;
            natural = 0;
            minimum_baseline = -1;
            natural_baseline = -1;

            return;
        }

        child.measure (
            orientation, for_size,
            out minimum, out natural,
            out minimum_baseline, out natural_baseline
        );
    }

    protected override void size_allocate (int width, int height, int baseline) {
        if (child == null)
            return;

        var t = new Gsk.Transform ();
        t = t.translate ({ width / 2, height / 2});

        if (transform != null)
            t = t.transform (transform);

        t = t.translate ({ -width / 2, -height / 2 });

        child.allocate (width, height, baseline, t);
    }
}
